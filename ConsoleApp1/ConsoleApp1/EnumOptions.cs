﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    enum EnumOptions
    {
        Create,
        Clear,
        Paint,
        DrawX,
        DrawY,
        FillR,
        Show,
        Terminate
    }
}
