﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Program
    {
        public Dictionary<string, int> pixeles { get; set; }
        public char[,] imagesArray { get; set; }

        public bool session { get; set; }

        public char pixel { get; set; }
        public char color { get; set; }
        public char command { get; set; }
        string[] textInput { get; set; }
        public Program() {

        }

        private void DynamicImage() {
            for (int i = 0; i < imagesArray.GetLength(0); i++)
            {

                if(command == 'I' || command == 'C')
                for (int j = 0; j < imagesArray.GetLength(1); j++)
                {
                    imagesArray[i, j] = 'O';
                }

                if ((command == 'L' || command == 'H' || command == 'F') && pixeles["y"] == i)
                {
                    for (int j = 0; j < imagesArray.GetLength(1); j++)
                    {
                        if (command == 'F' && pixeles["x"] == j)
                            pixel = imagesArray[i, j];

                        if ((command == 'L' && pixeles["x"] == j) || (command == 'H' && (j >= pixeles["x"] && j <= pixeles["x2"])))
                            imagesArray[i, j] = color;
                    }
                }
                if (command == 'V') {
                    if (i >= pixeles["y"] && i <= pixeles["y2"])
                    {
                        for (int j = 0; j < imagesArray.GetLength(1); j++)
                        {
                            if (pixeles["x"] == j)
                                imagesArray[i, j] = color;
                        }
                    }
                }
            }
        }

        private void Draw()
        {
            imagesArray = new char[pixeles["y"], pixeles["x"]];
        }

        private void InitializeVariables() {
            if (command == 'S' || command == 'T' || command == 'C')
                return;
            if (command == 'I')
            {
                pixeles.Add("x", Convert.ToInt32(textInput[1]));
                pixeles.Add("y", Convert.ToInt32(textInput[2]));
            }
            else {
                pixeles.Add("x", Convert.ToInt32(textInput[1])-1);
                pixeles.Add("y", Convert.ToInt32(textInput[2])-1);                
            }                       
        }

        public void Init() {
            command = Convert.ToChar(textInput[0]);
            pixeles = new Dictionary<string, int>();
            InitializeVariables();
            switch (command)
            {
                case 'I':                    
                    Draw();
                    DynamicImage();
                    break;
                case 'C':
                    DynamicImage();
                    break;
                case 'L':
                    color = textInput[3] != null ? Convert.ToChar(textInput[3]) : ' ';
                    DynamicImage();                    
                    break;
                case 'V':
                    pixeles.Add("y2",Convert.ToInt32(textInput[3]) - 1);
                    color = Convert.ToChar(textInput[4]);
                    DynamicImage();
                    break;
                case 'H':
                    pixeles.Add("x2", Convert.ToInt32(textInput[2]) - 1);
                    pixeles["y"] = Convert.ToInt32(textInput[3]) - 1;
                    color = Convert.ToChar(textInput[4]);
                    DynamicImage();
                    break;
                case 'F':
                    color = Convert.ToChar(textInput[3]);                    
                    DynamicImage();
                    for (int i = 0; i < imagesArray.GetLength(0); i++)
                    {
                        for (int j = 0; j < imagesArray.GetLength(1); j++)
                        {
                            if (pixel == imagesArray[i, j])
                                imagesArray[i, j] = color;
                        }
                    }
                    break;
                case 'S':
                    for (int i = 0; i < imagesArray.GetLength(0); i++)
                    {
                        string sector = "";
                        for (int j = 0; j < imagesArray.GetLength(1); j++)
                        {
                            sector += imagesArray[i, j];
                        }
                        Console.WriteLine(sector);
                    }
                    break;
                case 'T':
                    session = false;
                    break;
                default:
                    Console.WriteLine("Comando no reconocido.");
                    session = false;
                    break;
            }
        }
        static void Main(string[] args)
        {
            Program initialize = new Program();
            initialize.session = true;
            try
            {
                Console.WriteLine("Escribir el comando deseado con mayuscula, por favor.");
                while (initialize.session)
                {
                    string[] textInput = Console.ReadLine()?.Split(' ');
                    if ((textInput == null || textInput.Length <= 0))
                    {
                        Console.WriteLine("Por favor de introducir un comando correcto.");
                    }
                    else {
                        initialize.textInput = textInput;
                        initialize.Init();
                    }                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ha ocurrido un error interno, message: {0}", ex.Message);                
            }
        }
    }
}
